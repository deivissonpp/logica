public class FormulaMisteriosa {

	public static void main(String[] args) {
		String input = args[0];
		String output = "";
		while (input.length() != 0) {
			char item = input.charAt(0);
			int ocurrences = count(item, input);
			input = input.substring(ocurrences);
			output = output.concat(String.valueOf(ocurrences)).concat(String.valueOf(item));
		}

		System.out.println(output);
	}

	private static int count(char item, String input) {
		if (input.length() == 1) {
			return 1;
		}

		if (item == input.charAt(1)) {
			return 1 + count(item, input.substring(1));
		}

		return 1;
	}
}
