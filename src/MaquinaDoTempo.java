import java.util.Arrays;
import java.util.List;

public class MaquinaDoTempo {

	public static void main(String[] args) {
		boolean isValidDate = true;
		List<Integer> thirtyDaysMonths = Arrays.asList(4, 6, 9, 11);

		int year = Integer.parseInt(args[2]);
		int month = Integer.parseInt(args[1]);
		int day = Integer.parseInt(args[0]);

		if (day >= 1 && day <= 31) {
			if (month >= 1 && month <= 12) {
				if (month == 2 && day > 28 && day < 30 && !isLeapYear(year)) {
					isValidDate = false;
				} else if (thirtyDaysMonths.contains(month) && day > 30) {
					isValidDate = false;
				}
			} else {
				isValidDate = false;
			}

		} else {
			isValidDate = false;
		}

		System.out.println(isValidDate);
	}

	private static boolean isLeapYear(int year) {
		if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)) {
			return true;
		}
		return false;
	}
}
